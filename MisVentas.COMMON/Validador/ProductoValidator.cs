﻿using FluentValidation;
using MisVentas.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace MisVentas.COMMON.Validador
{
    public class ProductoValidator:GenericValidator<Producto>
    {
        public ProductoValidator()
        {
            RuleFor(entidad => entidad.Costo).NotNull().GreaterThan(0);
            RuleFor(entidad => entidad.Descripcion).NotNull().MinimumLength(10);
            RuleFor(entidad => entidad.Existencia).NotNull().GreaterThanOrEqualTo(0);
            RuleFor(entidad => entidad.Nombre).NotNull().MinimumLength(10);
        }
    }
}
