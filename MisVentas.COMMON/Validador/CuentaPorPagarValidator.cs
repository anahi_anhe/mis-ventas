﻿using FluentValidation;
using MisVentas.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace MisVentas.COMMON.Validador
{
    public class CuentaPorPagarValidator:GenericValidator<CuentaPorPagar>
    {
        public CuentaPorPagarValidator()
        {
            RuleFor(entidad => entidad.FechaLimiteDePago).NotNull().GreaterThan(DateTime.Now);
            RuleFor(entidad => entidad.Monto).NotNull().GreaterThan(0);
            RuleFor(entidad => entidad.Proveedor).NotNull().MinimumLength(10);
        }
    }
}
