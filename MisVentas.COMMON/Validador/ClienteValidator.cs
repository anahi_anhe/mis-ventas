﻿using FluentValidation;
using MisVentas.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace MisVentas.COMMON.Validador
{
    public class ClienteValidator:GenericValidator<Cliente>
    {
        public ClienteValidator()
        {
            RuleFor(entidad => entidad.Direccion).NotNull().MinimumLength(10);
            //RuleFor(entidad=>entidad.Image)
            RuleFor(entidad => entidad.Nombre).NotNull().MinimumLength(10);
            RuleFor(entidad=>entidad.ReferenciaDireccion).NotNull().MinimumLength(10);
            RuleFor(entidad=>entidad.ReferenciaNombre).NotNull().MinimumLength(10);
            RuleFor(entidad=>entidad.ReferenciaTelefono).NotNull().MinimumLength(10);
            RuleFor(entidad => entidad.Saldo).NotNull();
            RuleFor(entidad => entidad.Telefono).NotNull().Length(10);

        }
    }
}
