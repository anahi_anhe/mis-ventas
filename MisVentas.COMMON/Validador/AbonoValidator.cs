﻿using FluentValidation;
using MisVentas.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace MisVentas.COMMON.Validador
{
    public class AbonoValidator:GenericValidator<Abono>
    {
        public AbonoValidator()
        {
            RuleFor(entidad => entidad.IdCliente).NotNull().NotEmpty();
            RuleFor(entidad => entidad.Monto).NotNull().GreaterThan(0).WithMessage("El monto del abono debe ser mayor a 0")
            RuleFor(entidad => entidad.Notas).NotNull();
        }
    }
}
