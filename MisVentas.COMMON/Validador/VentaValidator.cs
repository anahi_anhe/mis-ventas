﻿using FluentValidation;
using MisVentas.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace MisVentas.COMMON.Validador
{
    public class VentaValidator:GenericValidator<Venta>
    {
        public VentaValidator()
        {
            RuleFor(entidad => entidad.Cantidad).NotNull().GreaterThan(0);
            RuleFor(entidad => entidad.Monto).NotNull().GreaterThan(0);
            RuleFor(entidad => entidad.IdCliente).NotNull().NotEmpty();
            RuleFor(entidad => entidad.IdProducto).NotNull().NotEmpty();
            RuleFor(entidad => entidad.Precio).NotNull().GreaterThan(0);
        }
    }
}
