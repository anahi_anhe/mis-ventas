﻿using FluentValidation;
using MisVentas.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace MisVentas.COMMON.Validador
{
    public class PedidoValidator: GenericValidator<Pedido>
    {
        public PedidoValidator()
        {
            RuleFor(entidad => entidad.Atendido).NotNull();
            RuleFor(entidad => entidad.Cantidad).NotNull().GreaterThan(0);
            RuleFor(entidad => entidad.Notas).NotNull();
        }
    }
}
