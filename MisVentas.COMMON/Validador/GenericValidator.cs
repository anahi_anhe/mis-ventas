﻿using FluentValidation;
using MisVentas.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace MisVentas.COMMON.Validador
{
    public class GenericValidator<T>:AbstractValidator<T> where T:BaseDTO
    {
        public GenericValidator()
        {
            RuleFor(entidad => entidad.Id).NotNull().NotEmpty();
            RuleFor(entidad => entidad.FechaHora).NotNull().NotEmpty();
        }
    }
}
