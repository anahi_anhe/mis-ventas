﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MisVentas.COMMON.Entidades
{
    public class Venta:BaseDTO
    {
        public string IdCliente { get; set; }
        public string IdProducto { get; set; }
        public int Cantidad { get; set; }
        public float Precio { get; set; }
        public float Monto { get; set; }
    }
}
