﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MisVentas.COMMON.Entidades
{
    public class Cliente:BaseDTO
    {
        public string Nombre { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }
        public Byte[] Image { get; set; }
        public float Saldo { get; set; }
        public string ReferenciaNombre { get; set; }
        public string ReferenciaDireccion { get; set; }
        public string ReferenciaTelefono { get; set; }
    }
}
