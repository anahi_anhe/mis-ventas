﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MisVentas.COMMON.Entidades
{
    public class CuentaPorPagar:BaseDTO
    {
        public DateTime FechaPagoReal { get; set; }
        public DateTime FechaLimiteDePago { get; set; }
        public float Monto { get; set; }
        public string Proveedor { get; set; }
    }
}
