﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MisVentas.COMMON.Entidades
{
    public class Abono:BaseDTO
    {
        public string IdCliente { get; set; }
        public float Monto { get; set; }
        public string Notas { get; set; }
    }
}
