﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MisVentas.COMMON.Entidades
{
    public class Producto:BaseDTO
    {
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public int Existencia { get; set; }
        public float Costo { get; set; }
    }
}
