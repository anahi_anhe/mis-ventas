﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MisVentas.COMMON.Entidades
{
    public class Pedido:BaseDTO
    {
        public string IdCliente { get; set; }
        public string IdProducto { get; set; }
        public float Cantidad { get; set; }
        public string Notas { get; set; }
        public bool Atendido { get; set; }
    }
}
