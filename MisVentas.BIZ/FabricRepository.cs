﻿using MisVentas.COMMON.Entidades;
using MisVentas.COMMON.Validador;
using MisVentas.DAL;
using System;

namespace MisVentas.BIZ
{
    public static class FabricRepository
    {
        public static GenericRepository<Abono> AbonoRepository()
        {
            return new GenericRepository<Abono>(new GenericValidator<Abono>());
        }
        public static GenericRepository<CuentaPorPagar> CuentaPorPagarRepository()
        {
            return new GenericRepository<CuentaPorPagar>(new GenericValidator<CuentaPorPagar>());
        }

        public static GenericRepository<Cliente> ClienteRepository()
        {
            return new GenericRepository<Cliente>(new GenericValidator<Cliente>());
        }

        public static GenericRepository<Pedido> PedidoRepository()
        {
            return new GenericRepository<Pedido>(new GenericValidator<Pedido>());
        }

        public static GenericRepository<Producto> ProductoRepository()
        {
            return new GenericRepository<Producto>(new GenericValidator<Producto>());
        }

        public static GenericRepository<Venta> VentaRepository()
        {
            return new GenericRepository<Venta>(new GenericValidator<Venta>());
        }

    }
}
