﻿using FluentValidation.Results;
using LiteDB;
using MisVentas.COMMON.Entidades;
using MisVentas.COMMON.Validador;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace MisVentas.DAL
{
    public class GenericRepository<T> where T : BaseDTO
    {
        string DBName="MisVentas.db";
        GenericValidator<T> validator;
        public GenericRepository(GenericValidator<T> _validator)
        {
            validator = _validator;
        }

        public string Error { get; private set; }

        public T Create(T entidad)
        {
            try
            {
                entidad.Id = Guid.NewGuid().ToString();
                entidad.FechaHora = DateTime.Now;
                ValidationResult result = validator.Validate(entidad);
                if (result.IsValid)
                {
                    bool r;
                    using (var db = new LiteDatabase(new ConnectionString() { Filename = DBName }))
                    {
                        r = db.GetCollection<T>(typeof(T).Name).Insert(entidad);
                    }
                    if (r)
                    {
                        Error = "";
                        return entidad;
                    }
                    else
                    {
                        Error = "No se pudo insertar la entidad";
                        return null;
                    }
                }
                else
                {
                    Error = "Datos invalidos: ";
                    foreach (var error in result.Errors)
                    {
                        Error = $"\n{error.ErrorMessage}";
                    }
                    return null;
                }
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return null;
            }
        }

        public IEnumerable<T> Read { get 
            {
                List<T> datos;
                using(var db=new LiteDatabase(new ConnectionString() { Filename = DBName }))
                {
                    datos = db.GetCollection<T>(typeof(T).Name).FindAll().ToList();
                }
                return datos;
            } 
        }

        public T Update(T entidad)
        {
            try
            {
                entidad.FechaHora = DateTime.Now;
                ValidationResult result = validator.Validate(entidad);
                if (result.IsValid)
                {
                    bool r;
                    using (var db = new LiteDatabase(new ConnectionString() { Filename = DBName }))
                    {
                        r = db.GetCollection<T>(typeof(T).Name).Update(entidad);
                    }
                    if (r)
                    {
                        Error = "";
                        return entidad;
                    }
                    else
                    {
                        Error = "No se pudo insertar la entidad";
                        return null;
                    }
                }
                else
                {
                    Error = "Datos invalidos: ";
                    foreach (var error in result.Errors)
                    {
                        Error = $"\n{error.ErrorMessage}";
                    }
                    return null;
                }
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return null;
            }
        }

        public bool Delete(T entidad)
        {
            try
            {
                bool r;
                using(var db = new LiteDatabase(new ConnectionString() { Filename = DBName }))
                {
                    r = db.GetCollection<T>(typeof(T).Name).Delete(entidad.Id);
                }
                Error = "";
                if (!r)
                {
                    Error = "No se pudo eliminar el registro";
                }
                return r;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return false;
            }
        }

        public T SearchById(string id)
        {
            try
            {
                T entidad;
                using (var db = new LiteDatabase(new ConnectionString() { Filename = DBName }))
                {
                    entidad = db.GetCollection<T>(typeof(T).Name).FindById(id);
                }
                Error = "";
                if (entidad!=null)
                {
                    Error = "No se pudo eliminar el registro";
                }
                return entidad;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return null;
            }
        }

        public IEnumerable<T> Query(Expression<Func<T,bool>> predicado)
        {
            try
            {
                IEnumerable<T> entidad;
                using (var db = new LiteDatabase(new ConnectionString() { Filename = DBName }))
                {
                    entidad = db.GetCollection<T>(typeof(T).Name).Find(predicado);
                }
                Error = "";
                if (!r)
                {
                    Error = "No se pudo eliminar el registro";
                }
                return entidad;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return null;
            }
        }
    }
}
